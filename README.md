> Licensed under http://www.apache.org/licenses/LICENSE-2.0
Attribution notice: by Stefan Müller in 2013 ff. (© http://badw.de)

# Documentation of Geist

<a href="public/icons/exempla.jpg"><img src="public/icons/exempla_min.jpg"/></a>

## How to run Geist

1. You need Python 3.8 or newer to be installed. On many computers, this requirement is already fulfilled. In order to try, open a shell or terminal and type a command that we call **PYTHONCALL** henceforth:

	- On Linux, type ``python3`` as PYTHONCALL, then Enter. This will most probably work, i.e. open a Python shell. You may leave the shell by typing ``exit()`` and Enter. If an error message appears, however, you need to install Python via your package manager.
	- On Windows, type ``py`` as PYTHONCALL, then Enter. If this works, it will open a Python shell. You may leave the shell by typing ``exit()`` and Enter. If an error message appears, however, you need to install Python via <https://python.org>. Make sure to check “Add Python ⟦…⟧ to PATH” at the bottom on the first installation screen. (You may also uncheck “Install launcher for all users” above so that you donʼt need administrator rights.)

2. Let’s assume a folder “NN” wherein you may have subfolders and files of a project called “NN”. To keep the files for the app apart, make a subfolder “app” in “NN”, i.e. “NN/app”. **Letʼs call the full path to NN: “PATH/TO/NN”.**

	**Note: On Windows, the delimiter in filepaths is the backslash (``\``) instead of the slash (``/``). Here, we give paths always with the slash; please, adapt accordingly.**

3. Into “PATH/TO/NN/app”, copy this repository, i.e. <https://gitlab.lrz.de/badw-it/geist>. Rename its folder from “geist” into “data”, so you have a folder “PATH/TO/NN/app/data”.

4. Also into “PATH/TO/NN/app”, copy the repository <https://gitlab.lrz.de/badw-it/system>, so you have a folder “PATH/TO/NN/app/system”.

5. Install python packages (i.e. programs) needed for Geist: Open a shell or terminal, type ``PYTHONCALL -m pip install -r "PATH/TO/NN/app/system/code/requirements.txt"`` (replacing “PYTHONCALL” and “PATH/TO/NN”) and press enter. (The file given after ``install -r`` contains the package names.) This command uses the python package installer “pip”, which should be included in your python installation – if not, please install it: see <https://pypi.org/project/pip/>.

6. Start the system: In the shell or terminal, type ``PYTHONCALL "PATH/TO/NN/app/system/code/NN/geist.py" "PATH/TO/NN/app/data/config.ini" "PATH/TO/NN/app/data/config_test.ini"`` (replacing “PYTHONCALL” and “PATH/TO/NN”) and press enter. – This is meant to exemplify a test run (as indicated by the name *config_test.ini*). In and for the long run you would probably use a service file; that is detailed in the next chapter.

7. If the port given in ``NN/app/data/config_test.ini`` is opened by the firewall and not used by another application, then this application’s first page should be accessible via browser at <http://localhost:8080>.

## How to set up a web server for running Geist in public

For local testing, i.e. for running the system on your computer at hand, the previous section suffices.

<details>

<summary>For running the system on a web server machine, you could set up this machine roughly as follows (click here to expand or collapse) …</summary>

- The operating system may be **Debian-Linux** with:

	- a user group “srvadm” with a homonymous user “srvadm”, who can act as an admin by prepending `sudo` to commands.
	- **mariadb** (database)
	- **nginx** (web server)
	- **python 3** (programming language)
	- **ufw** (firewall to open, show (via `sudo ufw status numbered`) and close ports)

- The machine gets a separate, large content partition mapped to a folder called **/local** henceforth. Into this folder we put:

	- one folder for each app. The path of such a folder is the path called “PATH/TO/NN” above. In the setup here, this path would be **/local/NN**.

	- the folder **/local/mariadb**, which will contain all database content.

	- the file **/local/README.md**, which references the documentation at hand.
		<details><summary>E.g.</summary>

			Auf dem vorliegenden Rechner sind Anwendungen für BAdW-Vorhaben in Betrieb.

			Die Anwendungen sind im Rahmenwerk Geist geschrieben.

			Geist und die Einrichtung des vorliegenden Rechners sind erläutert auf der Seite
			**<https://gitlab.lrz.de/badw-it/geist>**

			Diese Beschreibung ergänzend sei nur noch vermerkt:

			- Der Ordner **/local/web2py** enthält alte Eingabeseiten für gewisse Vorhaben –
			nämlich /local/kdih und /local/pal – und kann gelöscht werden, wenn diese zwei
			Eingabeseiten nicht mehr benötigt werden.

		</details>

	- the swapfile **/local/swapfile**.

- The folder **/etc/mysql** contains the database settings. Ensure:

	- in mariadb.conf.d/50-server.cnf in `[mysqld]`:

		- `datadir              = /local/mariadb/mysql`
		- `socket               = /run/mysqld/mysqld.sock`
		- `innodb_file_format   = Barracuda`
		- `character-set-server = utf8mb4`
		- `collation-server     = utf8mb4_unicode_520_ci`
		- `init-connect         = 'SET NAMES utf8mb4'`

	- in mariadb.conf.d/50-client.cnf in `[client]`: `default-character-set = utf8mb4`

- The file **/etc/nginx/conf.d/geist.conf** gets created to contain the configuration for the Nginx server, mainly all ports and domains and their mapping to the apps exposed to the public.

	<details><summary>E.g.</summary>

		# Licensed under http://www.apache.org/licenses/LICENSE-2.0
		# Attribution notice: by Stefan Müller in 2023 ff. (© http://badw.de)

		# INFORMATION
		## SECURITY: Every given “location” must end with a “/”.
		## NECESSITY: Both ip4 and ip6 must have a line, because multiple vhost domains are used; cf. https://serverfault.com/questions/638367#answer-685266.
		## CONVENTION:
		### Nginx should redirect queries from port 80 to port 443.
		### Nginx should pass queries from port 443 to apps listening on a 7000er port. These are the real apps in contrast to the test apps.
		### Example: ptolemaeus.badw.de:80 → ptolemaeus.badw.de:443 → real app on port 7004.
		### Nginx should pass queries from an 8000er port to apps listening on a 9000er port. These are the test apps.
		### The 8000er port should equal the 7000er port plus 1000, and the 9000er should equal the 8000er port plus 1000.
		### Example: ptolemaeus.badw.de:8004 → test app on port 9004.
		### Nginx should use geist.badw.de and badwver-geist.srv.mwn.de only temporarily for new apps under development.

		# Varia:
		client_max_body_size 4000M;
		gzip_vary on;
		gzip_proxied any;
		gzip_comp_level 6;
		gzip_buffers 16 8k;
		gzip_http_version 1.1;
		gzip_min_length 256;
		gzip_types
		application/javascript
		application/json
		application/xml
		image/svg+xml
		text/css
		text/javascript
		text/plain
		text/xml;

		# SSL:
		ssl_certificate     /etc/ssl/badwit.pem;
		ssl_certificate_key /etc/ssl/badwit.key;

		# Return 301 or 302:
		server {
			listen 80;
			listen [::]:80;
			listen 443 ssl;
			listen [::]:443 ssl;
			server_name jordanus.badw.de;
			return 301 $scheme://ptolemaeus.badw.de/jordanus;
		}
		server {
			listen 80;
			listen [::]:80;
			listen 443 ssl;
			listen [::]:443 ssl;
			server_name www.schelling.badw.de;
			return 301 https://schelling.badw.de$request_uri;
		}
		server {
			listen 80;
			listen [::]:80;
			listen 443 ssl;
			listen [::]:443 ssl;
			server_name www.geschichtsquellen.de;
			return 301 https://geschichtsquellen.de$request_uri;
		}
		server {
			listen 80 default_server;
			listen [::]:80 default_server;
			server_name _;
			return 302 https://$host$request_uri;
		}

		# Port 443 → 7000er Port:
		server {
			listen 443 ssl;
			listen [::]:443 ssl;
			server_name publikationen.badw.de archiv.badw.de cglo.badw.de tll-open.badw.de;
			location / {
				proxy_pass http://localhost:7001;
				include proxy_params;
			}
		}
		server {
			listen 443 ssl;
			listen [::]:443 ssl;
			server_name schelling.badw.de;
			location / {
				proxy_pass http://localhost:7002;
				include proxy_params;
			}
		}
		server {
			listen 443 ssl;
			listen [::]:443 ssl;
			server_name geschichtsquellen.de;
			location / {
				proxy_pass http://localhost:7003;
				include proxy_params;
			}
		}
		server {
			listen 443 ssl;
			listen [::]:443 ssl;
			server_name ptolemaeus.badw.de;
			location / {
				proxy_pass http://localhost:7004;
				include proxy_params;
			}
		}
		server { # Currently unused! Instead: kdih.badw.de → Typo3-VM, /datenbank → http://badwver-geist.srv.mwn.de:7005/datenbank/.
			listen 443 ssl;
			listen [::]:443 ssl;
			server_name kdih.badw.de;
			location / {
				proxy_pass http://localhost:7005;
				include proxy_params;
			}
		}
		server {
			listen 443 ssl;
			listen [::]:443 ssl;
			server_name dienst.badw.de;
			location / {
				proxy_pass http://localhost:7006;
				include proxy_params;
			}
		}

		# 8000er Port → 9000er Port:
		## Test:
		server {
			listen 8003 ssl;
			listen [::]:8003 ssl;
			server_name geschichtsquellen.de;
			error_page 497 301 =307 https://$host:$server_port$request_uri; # http-to-https on the same port.
			location / {
				proxy_pass http://localhost:9003;
				include proxy_params;
			}
		}
		server {
			listen 8004 ssl;
			listen [::]:8004 ssl;
			server_name ptolemaeus.badw.de;
			error_page 497 301 =307 https://$host:$server_port$request_uri; # http-to-https on the same port.
			location / {
				proxy_pass http://localhost:9004;
				include proxy_params;
			}
		}

		## Web2Py:
		server {
			listen 8888 ssl;
			listen [::]:8888 ssl;
			server_name ptolemaeus.badw.de badwver-geist.srv.mwn.de; # for pal and kdih; no alias for kdih as kdih.badw.de is another VM.
			error_page 497 301 =307 https://$host:$server_port$request_uri; # http-to-https on the same port.
			location / {
				proxy_pass http://localhost:9888;
				include proxy_params;
			}
		}

	</details>

- The folder **/etc/ssl** contains the key- and pem-files for https.

- The folder **/etc/systemd/system** contains all systemd service files to start, stop and restart the apps, e.g. a file **NN.service**.

	<details><summary>E.g.</summary>

		[Unit]
		Description=... (What NN is or does.)
		After=mysql.service postfix.service sshd.service

		[Service]
		User=srvadm
		Group=srvadm
		Type=simple
		ExecStart=PYTHONCALL "PATH/TO/NN/app/system/code/NN/geist.py" "PATH/TO/NN/app/data/config.ini" "PATH/TO/NN/app/data/config_real.ini"

		[Install]
		WantedBy=multi-user.target

	</details>

	To install this service, `sudo systemctl enable /etc/systemd/system/NN.service` was issued.

	Now you can stop NN with `sudo systemctl stop NN`, start or restart it with `sudo systemctl restart NN` and ask about its status with `sudo systemctl status NN`. Furthermore, NN is started automatically after a reboot.

	Besides, the value of `ExecStart` here is the start command that I introduced in the previous chapter, but this time with `config_real.ini` instead of `config_test.ini`.

- With crontab, **cronjobs** are shown (`sudo crontab -l`) and edited (`sudo crontab -e`). You may let a cronjob first e.g. delete log files (for data protection) and then reboot the machine once a week in the early morning: `30 3 * * tue rm /var/log/nginx/* && /sbin/shutdown -r now`

</details>

## Overview of the folders and files of Geist

- “□” signals folders, “▪” signals files.
- Names written in quotation marks are examples, which may easily differ from app to app.
- Parts in parentheseis are descriptions, not names of folders or files.

### Abridged overview:

1. □ “NN” (this folder may contain anything besides the app, e.g. contracts, todo- or address-lists)
	1. □ app (**non-public level**)
		1. □ data (**repo-public level**: folder for data specific to this app)
			1. □ public (**web-public level**: folder for the content of this app)
			2. ▪ (config files)
		2. □ system (**repo-public level**: folder for code used for this app and other ones)
			1. □ code (not delivered to browsers)
			2. □ cssjs (**web-public level**: CSS- or JS-files delivered to browsers and executed there)

### Comprehensive overview:

1. □ “NN” (this folder may contain anything besides the app, e.g. contracts, todo- or address-lists)
	1. □ app (**non-public level**)
		1. □ data (**repo-public level**: folder for data specific to “NN”)
			1. □ .git
			2. □ public (**web-public level**: folder for the content of “NN”)
				1. □ (folders for e.g. icons, images, fonts)
				2. ▪ (files whose name is the name of a webpage in the URL and whose content is the main content of this page)
			3. □ (folders for data not accessible via web)
			4. ▪ config.ini
			5. ▪ (further config files, e.g. “config_real.ini”, “config_test.ini”)
			6. ▪ glosses.ini (for multilingual websites)
			7. ▪ README.md
		2. □ system (**repo-public level**: folder for code used for all apps)
			1. □ .git
			2. □ code (not delivered to browsers)
				1. □ gestalt (*views*)
					1. ▪ (templates for all apps)
				2. □ “NN”
					1. □ gestalt (*views*)
						1. ▪ (templates for the app “NN”)
					2. ▪ geist.py (*controller*)
					3. ▪ gehalt.py (*model*)
					4. ▪ (further python modules for “NN”)
					5. ▪ requirements.txt (of the app “NN”)
				3. □ (other apps apart from “NN”)
				4. ▪ requirements.txt (of multiple apps)
				5. ▪ (general python modules for all apps)
			3. □ cssjs (**web-public level**: CSS- or JS-files delivered to browsers and executed there)
				1. ▪ “NN.css”
				2. ▪ “NN.js”
			4. ▪ .gitignore
			5. ▪ README.md
		3. □ (automatically generated folders for temporary files, to wit logs and sessions)
		4. ▪ auth.json (if you need authentication, more about that below)

## How to adapt the system

Adapt your copy of the system by changing folders or files whose name or content contains “NN”. Particularly:

- If you need authentification:
	- Copy the following JSON text:

			{"next_id": 2, "data": [{"id": 1, "name": "NN", "hash": [73, 239, 15, 199, 8, 252, 210, 106, 109, 118, 117, 103, 143, 137, 137, 135, 237, 23, 127, 97, 59, 101, 32, 210, 129, 59, 148, 43, 11, 150, 144, 5, 98, 152, 144, 175, 198, 33, 24, 246, 214, 211, 36, 8, 253, 43, 166, 221, 75, 186, 65, 57, 153, 140, 65, 251, 110, 96, 12, 244, 65, 12, 10, 7, 70, 36, 76, 124, 225, 176, 247, 4, 152, 79, 135, 107, 202, 100, 64, 160, 54, 113, 21, 42, 33, 133, 200, 149, 19, 88, 87, 200, 228, 40, 19, 232, 75, 222, 223, 215, 42, 95, 117, 91, 10, 172, 91, 94, 249, 7, 235, 202, 91, 39, 88, 161, 223, 130, 18, 107, 233, 29, 71, 36, 1, 182, 247, 27], "salt": [247, 161, 29, 31, 146, 96, 178, 151, 200, 214, 84, 218, 99, 108, 98, 107, 112, 35, 3, 101, 33, 139, 45, 58, 91, 31, 242, 98, 180, 179, 47, 154, 203, 157, 11, 89, 199, 68, 65, 218, 140, 226, 145, 173, 198, 171, 138, 56, 24, 78, 144, 103, 94, 5, 91, 172, 90, 163, 20, 54, 54, 107, 254, 111, 247, 70, 78, 57, 179, 76, 124, 215, 197, 136, 175, 95, 82, 109, 77, 171, 80, 47, 160, 226, 96, 30, 175, 208, 92, 105, 41, 142, 27, 116, 103, 7, 157, 134, 165, 181, 88, 145, 22, 124, 106, 9, 125, 91, 247, 185, 105, 184, 148, 185, 151, 18, 75, 34, 70, 133, 192, 99, 15, 181, 112, 91, 95, 183], "roles": ["administrator", "editor", "redactor", "inspector"]}]}

	- Paste it into a file, name the file “auth.json” and save it in the parent folder of the folder containing this README. The file does not contain passwords in plain text, of course, but should be kept non-public nonetheless. – You can save the file at another location; then write the new path into the config.ini under section “paths_from_config_folder” under the key “auth_doc” as a relative path seen from the folder of the config.ini itself.
	- Before you run the system at a port on a machine which are open to access from the outside: run the system as described above; go to <http://localhost:8080/de/auth_in>, log in as “NN” with the password “NN”, go to <http://localhost:8080/de/auth_admin> and change the name “NN” and set a new, more secure, especially much, much longer password. The credentials are stored in the aforementioned file “auth.json”. Copy this file into the corresponding folder on the machine that is or will be publicly accessible. The new credentials (and only these) are then effective when you run the system on said machine.
- Adapt the config files “config.ini” et cetera.
- Adapt the controller “geist.py”: Modify and add routes and their routines. According to what the controller needs now, adapt the model “gehalt.py” and the templates in the folder “gestalt”. **The framework used for routing and templating is Bottle, cf. <https://bottlepy.org>.**
- Adapt the CSS and – if necessary – the JS in the folder “cssjs”. **The framework used for interactive tables is Datatables, cf. <https://www.datatables.net/>.**
- If you need new glosses, add them to “glosses.ini”.

## Examples for sites made with Geist

- <https://geschichtsquellen.de>
- <https://schelling.badw.de>
- <https://kdih.badw.de/datenbank>
- <https://ptolemaeus.badw.de> with <a href="https://ptolemaeus.badw.de/work/3">catalog</a>, <a href="https://ptolemaeus.badw.de/ms/668/970/transcription/1">transcriptions</a>, <a href="https://ptolemaeus.badw.de/ms/2/2/82v">facsimilia &amp; text</a>.
- <https://jordanus.badw.de>
- <https://publikationen.badw.de>. Examples for subsites:
	- <https://publikationen.badw.de/de/F1/text>
	- <https://publikationen.badw.de/de/rla/index>
	- <https://publikationen.badw.de/de/cglo/index>
	- <https://publikationen.badw.de/de/bwb/index>
	- <https://publikationen.badw.de/de/schmeller/index>
	- <https://publikationen.badw.de/de/thesaurus/lemmata>
	- <https://publikationen.badw.de/de/A1/abb>
- <https://archiv.badw.de>
- <https://dienst.badw.de>
- <https://quellen.perspectivia.net> is a multisite comprising:
	- <https://quellen.perspectivia.net/borchward>
	- <https://quellen.perspectivia.net/fantin-scholderer>
	- <https://quellen.perspectivia.net/judenporzellan>
	- <https://quellen.perspectivia.net/russische_relationen>
	- <https://quellen.perspectivia.net/schatullrechnungen>
